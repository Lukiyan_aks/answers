#!/usr/bin/env python3
import hashlib,sys,string,random,argparse,getopt

def md5_hash(N_rg):
    MyString=''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(N_rg))
    MyMd5hash=hashlib.md5(MyString.encode('utf-8')).hexdigest()
    return MyMd5hash,MyString

def check_args(argv):
    try:
        opts, args = getopt.getopt(argv,"hm:n:f:")
    except getopt.GetoptError:
        print("md5_1.py -m <first byte> -n <string's lenght> -f <file name>")
        sys.exit(2)  
    B_tb = "00"
    N_rg = 10
    F_name = "randomfile.txt"
    for opt, arg in opts:
        if opt == '-h':
            print("md5_1.py -m <first byte> -n <string's lenght> -f <file name>")
            sys.exit()
        elif opt in ("-m"):
            B_tb = arg
        elif opt in ("-n"):
            N_rg= int(arg)
        elif opt in ("-f"):
            F_name = arg
    return N_rg, B_tb, F_name

if __name__ == "__main__":
    N_rg, B_tb, F_name = check_args(sys.argv[1:])
    print(F_name)
    MyMd5hash = ""
    while MyMd5hash[:2] != B_tb:
        MyMd5hash,MyString = md5_hash(N_rg)
    MyOut = MyMd5hash + " " +MyString 
    print(MyOut)
    with open(F_name, 'w') as MyFile:
        MyFile.write(MyOut)
    MyFile.close()